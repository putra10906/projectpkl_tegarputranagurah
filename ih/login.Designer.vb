﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class login
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Label2 = New Label()
        TextBox2 = New TextBox()
        Label1 = New Label()
        TextBox1 = New TextBox()
        Button3 = New Button()
        registbut2 = New Button()
        loginbut2 = New Button()
        SuspendLayout()
        ' 
        ' Label2
        ' 
        Label2.AutoSize = True
        Label2.Location = New Point(377, 247)
        Label2.Name = "Label2"
        Label2.Size = New Size(57, 15)
        Label2.TabIndex = 13
        Label2.Text = "Password"
        ' 
        ' TextBox2
        ' 
        TextBox2.BorderStyle = BorderStyle.FixedSingle
        TextBox2.Location = New Point(291, 269)
        TextBox2.Name = "TextBox2"
        TextBox2.Size = New Size(221, 23)
        TextBox2.TabIndex = 12
        ' 
        ' Label1
        ' 
        Label1.AutoSize = True
        Label1.Location = New Point(377, 154)
        Label1.Name = "Label1"
        Label1.Size = New Size(60, 15)
        Label1.TabIndex = 11
        Label1.Text = "Username"
        ' 
        ' TextBox1
        ' 
        TextBox1.BorderStyle = BorderStyle.FixedSingle
        TextBox1.Location = New Point(291, 177)
        TextBox1.Name = "TextBox1"
        TextBox1.Size = New Size(221, 23)
        TextBox1.TabIndex = 10
        ' 
        ' Button3
        ' 
        Button3.BackColor = Color.NavajoWhite
        Button3.Cursor = Cursors.Hand
        Button3.Font = New Font("Segoe UI", 9F, FontStyle.Bold, GraphicsUnit.Point)
        Button3.Location = New Point(332, 336)
        Button3.Name = "Button3"
        Button3.Size = New Size(160, 35)
        Button3.TabIndex = 9
        Button3.Text = "Login"
        Button3.UseVisualStyleBackColor = False
        ' 
        ' registbut2
        ' 
        registbut2.BackColor = Color.NavajoWhite
        registbut2.Cursor = Cursors.Hand
        registbut2.Font = New Font("Segoe UI", 9F, FontStyle.Bold, GraphicsUnit.Point)
        registbut2.Location = New Point(402, 80)
        registbut2.Name = "registbut2"
        registbut2.Size = New Size(177, 35)
        registbut2.TabIndex = 8
        registbut2.Text = "Register"
        registbut2.UseVisualStyleBackColor = False
        ' 
        ' loginbut2
        ' 
        loginbut2.BackColor = Color.NavajoWhite
        loginbut2.Cursor = Cursors.Hand
        loginbut2.Font = New Font("Segoe UI", 9F, FontStyle.Bold, GraphicsUnit.Point)
        loginbut2.Location = New Point(221, 80)
        loginbut2.Name = "loginbut2"
        loginbut2.Size = New Size(177, 35)
        loginbut2.TabIndex = 7
        loginbut2.Text = "Login"
        loginbut2.UseVisualStyleBackColor = False
        ' 
        ' login
        ' 
        AutoScaleDimensions = New SizeF(7F, 15F)
        AutoScaleMode = AutoScaleMode.Font
        ClientSize = New Size(800, 450)
        Controls.Add(Label2)
        Controls.Add(TextBox2)
        Controls.Add(Label1)
        Controls.Add(TextBox1)
        Controls.Add(Button3)
        Controls.Add(registbut2)
        Controls.Add(loginbut2)
        Name = "login"
        Text = "login"
        ResumeLayout(False)
        PerformLayout()
    End Sub

    Friend WithEvents Label2 As Label
    Friend WithEvents TextBox2 As TextBox
    Friend WithEvents Label1 As Label
    Friend WithEvents TextBox1 As TextBox
    Friend WithEvents Button3 As Button
    Friend WithEvents registbut2 As Button
    Friend WithEvents loginbut2 As Button
End Class
